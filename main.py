import models

from fastapi import FastAPI
from routes import route_quiz
from database import engine

app = FastAPI()
app.include_router(route_quiz)

models.Base.metadata.create_all(bind=engine)