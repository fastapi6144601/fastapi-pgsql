import models

from typing import Annotated
from fastapi import APIRouter
from sqlalchemy.orm import Session
from fastapi import HTTPException, Depends
from database import engine, SessionLocal
from schemas import QuestionBase, ChoiceBase



def get_db():
  db = SessionLocal()
  try:
    yield db
  finally:
    db.close()

db_dependency = Annotated [Session, Depends(get_db)]

route_quiz = APIRouter()

@route_quiz.get('/choice/{question_id}')
async def read_choice(question_id: int, db: db_dependency):
  result = db.query(models.Choices).filter(models.Choices.question_id == question_id).all()
  if not result:
    raise HTTPException(status_code=404, detail='Choices is not found')
  return result

@route_quiz.get('/choice')
async def read_choice(db: db_dependency):
  result = db.query(models.Choices).all()
  if not result:
    raise HTTPException(status_code=404, detail='Choices is not found')
  return result


@route_quiz.get('/question/{question_id}')
async def read_question(question_id: int, db: db_dependency):
  result = db.query(models.Questions).filter(models.Questions.id == question_id).first()
  if not result:
    raise HTTPException(status_code=404, detail='Question is not found')
  return result

@route_quiz.get('/question')
async def read_question(db: db_dependency):
  result = db.query(models.Questions).all()
  if not result:
    raise HTTPException(status_code=404, detail='Question is not found')
  return result

@route_quiz.post('/questions/')
async def create_question(question: QuestionBase, db: db_dependency):
  db_question = models.Questions(question_text=question.question_text)
  db.add(db_question)
  db.commit()
  db.refresh(db_question)
  for choice in question.choices:
    db_choice = models.Choices(choice_text=choice.choice_text, is_correct=choice.is_correct, question_id=db_question.id)
    db.add(db_choice)
  db.commit()

