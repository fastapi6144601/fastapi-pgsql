# Do this after git clone (Run the program with the command):
- python -m venv venv
- venv\Scripts\activate.bat (windows) / source venv/bin/activate (Mac/Linux)
- pip install -r requirements.txt
- Create a database.py file. The contents of the file are:

````
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

URL_DATABASE = 'postgresql://<username>:<password>@<server>/<username>'

engine = create_engine(URL_DATABASE)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
````

- uvicorn main:app --port 3000  --reload
